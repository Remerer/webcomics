﻿# Pepper&Carrot webcomics

This repository contains all the files necessary to translate the webcomic [Pepper&Carrot](http://www.peppercarrot.com).

## Documentation:

A simple how-to translate can be read [here](http://www.peppercarrot.com/fr/article267/how-to-add-a-translation-or-a-correction).

## Credits:

Read CONTRIBUTORS.md at the root for the full attribution, this file is public on the main website: ["Author" category](https://www.peppercarrot.com/static7/author).

## Sources artworks

This repository contains only low resolution artworks for editing translation. Do not edit or propose a commit for the artwork in these repository. For rendering the page at 300ppi, use the high resolution graphics available [on the main server.](http://www.peppercarrot.com/0_sources/).

## Fonts

The open fonts necessary to translate Pepper&Carrot are in the subfolder fonts. 
For documentation, license about them, read the fonts/README.md file.

# License:

Authors of all translations or contributions to this project accept to release this translation work under the license: [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
